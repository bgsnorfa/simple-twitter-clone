const express = require("express");
const bodyParser = require("body-parser");
const sessionHandler = require("express-session");
const cookieParser = require("cookie-parser");

const { User, Tweet } = require("./models");

const jsonParser = bodyParser.json();
const urlEncoded = bodyParser.urlencoded({ extended: false });

const app = express();

const oneDay = 1000 * 60 * 60 * 24;

app.use(sessionHandler({
    secret: "secret",
    saveUninitialized:true,
    cookie: { maxAge: oneDay },
    resave: false
}));

app.use(cookieParser());

app.set('view engine', 'ejs');

const port = "3000";

// Views
app.get("/", async (req, res) => {
    let tweets = await Tweet.findAll({
        include: User,
        order: [
            ['createdAt', 'DESC']
        ]
    });
    res.render('home', {
        session: req.session,
        tweets: tweets
    });
});

app.get("/tweet", (req, res) => {
    if (!req.session.userId) {
        return res.redirect('/');
    }

    res.render('tweets/create', {
        session: req.session
    });
});

app.get("/login", (req, res) => {
    if (req.session.userId) {
        return res.redirect('/');
    }

    res.render('users/login', {
        session: req.session
    });
});

app.get("/register", (req, res) => {
    if (req.session.userId) {
        return res.redirect('/');
    }

    res.render('users/register', {
        session: req.session
    });
});

// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

// API
app.post("/api/login", urlEncoded, async (req, res) => {
    let message = {
        type: 'error',
        message: 'Login failed, username/password does not match'
    };

    try {
        let user = await User.findOne({
            where: {
                username: req.body.username
            }
        });

        if (!user.validPassword(req.body.password)) {
            return res.render('users/login', {
                message: message
            })
        }

        // Store to session
        req.session.userId = user.id;
        res.redirect('/login');
    } catch (error) {
        res.render('users/login', {
            message: message
        })
    }
});

app.post("/api/register", urlEncoded, async (req, res) => {
    try {
        let user = await User.create({
            username: req.body.username,
            password: req.body.password,
        });

        res.redirect('/login');
    } catch (error) {
        res.render('users/register', {
            message: {
                type: 'error',
                message: error.errors[0].message
            }
        })
    }
});

app.get("/api/logout", urlEncoded, async (req, res) => {
    if (req.session.userId) {
        req.session.destroy();
        res.redirect('/');
    }
})

app.post("/api/tweet", urlEncoded, async (req, res) => {
    try {
        let tweet = await Tweet.create({
            userId: parseInt(req.session.userId),
            tweet: req.body.tweet
        });

        res.redirect('/');
    } catch (error) {
        res.json(error);
        res.render('tweets/create', {
            session: req.session,
            message: {
                type: 'error',
                message: error.errors[0].message
            }
        })
    }
});

app.listen(port, () => {
    console.table({
        url: `http://localhost:${port}`
    });
});